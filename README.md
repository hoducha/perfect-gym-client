# perfect-gym-client

A PHP Client for PerfectGym API.

The Client has 2 main methods: `authorize` and `request`.

**Authorize**
```php
string authorize($username, $password, $packedToken = null)
```
Authorization using username/password or an existing packed token. A packed token has structure like: `access_token|token_type|expire_timestamp`.

If a packed token is provided and valid then it will reuse that token, otherwise it will request a new token with username/password.

The method returns a packed token. It may be a new token or the old one if it is still valid.

**Request**
```php
object request($method, $endpoint, $options = [])
```
Sending authenticated request to the API endpoint.

The request method has same interface as the `request` method of [Guzzle](https://github.com/guzzle/guzzle).

## Installation
**Using Composer**
```
composer require hoducha/perfect-gym-client
```

## Usage
The usage is simple as bellow:
```php
use PerfectGym\Client;

// Init the client
$client = new Client('https://yoursubdomain.perfectgym.pl/Api');

// Authorize
$existingToken ='an-existing-token';
$token = $client->authorize('yourusername', 'yourpassword', $existingToken); 
// TODO: Store this token somewhere for reuse

// Send GET request example - list memberships
$res = $client->request('GET', '/Memberships/Memberships');
if ($res->getStatusCode() == 200) {
  $content = (string) $res->getBody();
  $r = json_decode($content, true);
  print_r($r);
}

// Send POST request example - add new user
$res2 = $client->request('POST', 'Users/User', [
  'form_params' => [
    'email' => 'hello@example.com',
    'homeClubId' => 10,
    'birthDate' => '1992-06-01T00:00:00',
    'firstName' => 'Hello'
  ]
]);
```

## Contributing

Pull Requests are very welcome!

If you find any issues, please report them via [Github Issues](https://github.com/hoducha/perfect-gym-client/issues)!