<?php
namespace PerfectGym;

class Client {
  private $accessToken;
  private $tokenType;
  private $guzzleClient;
  private $baseUrl;

  public function __construct($baseUrl) {
    $this->guzzleClient = new \GuzzleHttp\Client();
    $this->baseUrl = $baseUrl;
  }

  /**
   * Authorization using username/password or an existing packed token.
   * If a packed token is provided and valid then it will reuse that token, 
   * otherwise it will request a new token with username/password.
   * 
   * @param string $username
   * @param string $password
   * @param string $packedToken The packed token with structure like: access_token|token_type|expire_timestamp
   * @return string A packed token, it may be a new token or the old one if it is still valid
   */
  public function authorize($username, $password, $packedToken = null) {
    if ($packedToken) {
      list($token, $type, $expire) = explode('|', $packedToken);
      if ($token && $type && $expire && $expire > time()) {
        $this->accessToken = $token;
        $this->tokenType = $type;
        return $packedToken;
      }
    }
    if (!$this->accessToken) {
      $res = $this->guzzleClient->request('POST', $this->baseUrl . '/oauth/authorize', [
        'form_params' => [
          'username' => $username,
          'password' => $password,
          'grant_type' => 'password'
        ]
      ]);
      if ($res->getStatusCode() == 200) {
        $content = (string) $res->getBody();
        $r = json_decode($content, true);
        $this->accessToken = $r['access_token'];
        $this->tokenType = $r['token_type'];
        // Set the expire time 1 hour sooner the actual value 
        // to make sure it will not become invalid while processing the request
        $expire = time() + $r['expires_in'] - 3600;
        $newPackedToken = implode('|', [$this->accessToken, $this->tokenType, $expire]);
        return $newPackedToken;
      } else {
        throw new \Exception('Failed to authorize');
      }
    }
  }

  /**
   * Sending authenticated request to the API endpoint.
   * 
   * @param string $method The request method
   * @param string $endpoint The API endpoint (eg. /Memberships/Memberships)
   * @param array $options The request options
   * @return object The Psr-7 response object
   */
  public function request($method, $endpoint, $options = []) {
    $method = strtoupper($method);
    $url = $this->baseUrl . $endpoint;
    $options['headers'] = [
      'Authorization' => $this->tokenType . ' ' . $this->accessToken
    ];
    return $this->guzzleClient->request($method, $url, $options);
  }
}
