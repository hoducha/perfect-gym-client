<?php
namespace PerfectGym\Tests;

use PerfectGym\Client;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase {

  public function testAuthorize() {
    $username = getenv('USERNAME');
    $password = getenv('PASSWORD');
    $client = new Client(getenv('BASE_URL'));
    
    $token = $client->authorize($username, $password, null);
    $parts = explode('|', $token);
    $this->assertEquals(3, count($parts), 'Failed to authorize, it should return a packed token that has 3 parts separated by |');
    
    $token2 = $client->authorize($username, $password, $token);
    $this->assertEquals($token, $token2, 'It should reuse the existing token');

    $expiredToken = implode('|', [$parts[0], $parts[1], time()]);
    $token3 = $client->authorize($username, $password, $expiredToken);
    $this->assertNotEquals($expiredToken, $token3, 'It should request a new token if the given one is expired');
  }

  public function testAuthenticatedRequest() {
    $username = getenv('USERNAME');
    $password = getenv('PASSWORD');
    $client = new Client(getenv('BASE_URL'));
    $client->authorize($username, $password, null);
    $res = $client->request('GET', '/Memberships/Memberships');
    $this->assertEquals(200, $res->getStatusCode(), 'The request should pass the authorization');
  }
}